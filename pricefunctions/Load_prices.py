# -*- coding: utf-8 -*-
"""
Created on Nov 8 2023

Function to take elspot prices and return buy/sell prices

@author: haris
"""

def Load_prices(params):
    
    """
    params is a dictionary with default parameters
    params = {}
    params["Area"] = "Radius"
    params["Type"] = "C"
    params["taxes"] = True
    params["VAT"] = True
    params["Self_Con"] = False
    params["res"] = 60          # time resolution in minutes
    params["DK_zone"] = "DK2"
    """
    
    import holidays
    import os
    import pandas as pd
    from pytz import timezone
    from pricefunctions.AddTariffs import AddTariffs
    
    # Load prices
    file_P = os.path.join(os.getcwd(),'Data','Elspotprices.json')
    df_prices = pd.read_json(file_P)
    df_prices["HourUTC"] = pd.to_datetime(df_prices["HourUTC"])
    df_prices["HourDK"] = pd.to_datetime(df_prices["HourDK"])
    
    # Filter price zone
    df_prices = df_prices.loc[df_prices["PriceArea"]==params["DK_zone"]]
    
    # Sort values
    df_prices = df_prices.sort_values(by='HourUTC', ascending=True)
    
    # Add time info
    df_prices["HourUTC"] = df_prices["HourUTC"].dt.tz_localize(timezone('UTC'))
    df_prices["HourDK"] = df_prices["HourUTC"].dt.tz_convert(timezone('CET'))
    df_prices = df_prices.drop(columns=["PriceArea","SpotPriceEUR"])
    df_prices = df_prices.reset_index(drop=True)

    # Adjust prices by adding all the components
    
    # Get the list of Danish holidays for each year
    years = df_prices['HourDK'].dt.year.unique()
    dk_holidays = holidays.Denmark(years=years)
        
    # Create a new column 'is_holiday' and set it initially as False
    df_prices['is_holiday'] = False
    # Create a new column 'is_weekend' and set it initially as False
    df_prices['is_weekend'] = False
    # Indicate holidays and weekends
    df_prices['is_holiday'] = df_prices['HourDK'].map(lambda x: x in dk_holidays)
    df_prices.loc[df_prices['HourDK'].dt.weekday.isin([5, 6]), 'is_weekend'] = True 
    # Create a new column 'season' and set it initially as 'Winter'
    df_prices['season'] = 'Winter'
    # Update 'season' column to get 'Summer'
    df_prices.loc[df_prices['HourDK'].dt.month.isin([4, 5, 6, 7, 8, 9]), 'season'] = 'Summer'
        
    # Add tariffs
    df_prices = AddTariffs(df_prices, params)
        
    columns = ["HourUTC", "HourDK", "l_b", "SpotPriceDKK", "l_s", "DSO_ImportTariff"]
    df_prices = df_prices[columns + (["Self_Con"] if params["Self_Con"] else [])]
        
    # set the 'HourUTC' column as the index
    df_prices.set_index('HourUTC', inplace=True)
    
    # resample to params["res"] minute frequency and fill missing values with previous value
    ResT = str(params["res"]) + 'T'
    df_resampled = df_prices.resample(ResT).ffill()
    
    # reset index and add a new column with the corresponding HourDK value
    df_resampled.reset_index(inplace=True)
    df_resampled['HourDK'] = df_resampled['HourUTC'].dt.tz_convert('CET')
    df_resampled = df_resampled.iloc[0:-1]
    
    df_resampled = df_resampled.rename(columns={'HourUTC': 'TimeUTC','HourDK': 'TimeDK'})
    
    return df_resampled