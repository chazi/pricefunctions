# -*- coding: utf-8 -*-
"""
Created on Wed Nov  8 17:10:44 2023

Function to add tariffs on prices

@author: chazi
"""


def AddTariffs(df, params):
    """
    https://info.skat.dk/data.aspx?oid=2061620
    https://radiuselnet.dk/elnetkunder/tariffer-og-netabonnement/
    https://energinet.dk/El/Elmarkedet/Tariffer
    """ 
            
    # Apply tax, excl MOMS
    df["Tax"] = ""
    df.loc[(df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month<7),"Tax"] = 0.008
    df.loc[(df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month.isin([7,8,9])),"Tax"] = 0.697
    df.loc[(df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month>=10),"Tax"] = 0.8713
    df.loc[(df["HourDK"].dt.year==2022) & (df["HourDK"].dt.month<7),"Tax"] = 0.903
    df.loc[(df["HourDK"].dt.year==2022) & (df["HourDK"].dt.month>=7) & (df["HourDK"].dt.month<=9),"Tax"] = 0.763
    df.loc[(df["HourDK"].dt.year==2022) & (df["HourDK"].dt.month>=10),"Tax"] = 0.723
    df.loc[(df["HourDK"].dt.year<2022),"Tax"] = 0.9
            
    # Apply TSO tariffs on Imports, excl MOMS
    df["TSO_ImportTariff"] = ""
    df.loc[df["HourDK"].dt.year==2024,"TSO_ImportTariff"] = (7.4+5.1+0)/100
    df.loc[df["HourDK"].dt.year==2023,"TSO_ImportTariff"] = (5.8+5.4+0)/100
    df.loc[df["HourDK"].dt.year==2022,"TSO_ImportTariff"] = (4.9+6.1+0.229)/100
    df.loc[df["HourDK"].dt.year==2021,"TSO_ImportTariff"] = (4.9+6.1+0.229)/100
    df.loc[df["HourDK"].dt.year==2020,"TSO_ImportTariff"] = (5.3+4.4+0.187)/100
    df.loc[df["HourDK"].dt.year==2019,"TSO_ImportTariff"] = (4.4+3.6+0.163)/100
            
    # Apply TSO tariffs on Exports, excl MOMS
    df["TSO_ExportTariff"] = ""
    df.loc[df["HourDK"].dt.year>=2022,"TSO_ExportTariff"] = (0.3+0.116)/100
    df.loc[df["HourDK"].dt.year==2021,"TSO_ExportTariff"] = (0.3+0.123)/100
    df.loc[df["HourDK"].dt.year==2020,"TSO_ExportTariff"] = (0.3+0.112)/100
    df.loc[df["HourDK"].dt.year==2019,"TSO_ExportTariff"] = (0.3+0.086)/100
            
    # Apply DSO tariffs on Imports, excl MOMS
    
    if params["Area"] == "Radius":
                
        if params["Type"] == "C":
                    
            df["DSO_ImportTariff"] = ""
                    
            # Year 2019
            # April to September low price
            # October to March: low price expect for 17-20
                    
            # 1/1/19 until 31/7/19
            df.loc[(df["HourDK"].dt.year==2019) & (df["HourDK"].dt.month<=7),
                   "DSO_ImportTariff"] = 0.2589
            df.loc[(df["HourDK"].dt.month.isin([1,2,3])) & (df["HourDK"].dt.hour.isin([17,18,19])) &
                   (df["HourDK"].dt.year==2019), "DSO_ImportTariff"] = 0.668
                    
            # 1/8/19 until 31/10/19
            df.loc[(df["HourDK"].dt.year==2019) & (df["HourDK"].dt.month.isin([8,9,10])),
                   "DSO_ImportTariff"] = 0.2523
            df.loc[(df["HourDK"].dt.month.isin([10])) & (df["HourDK"].dt.hour.isin([17,18,19])) &
                   (df["HourDK"].dt.year==2019), "DSO_ImportTariff"] = 0.6614
                    
            # 1/11/19 until 31/12/19
            df.loc[(df["HourDK"].dt.year==2019) & (df["HourDK"].dt.month.isin([11,12])),
                   "DSO_ImportTariff"] = 0.2589
            df.loc[(df["HourDK"].dt.month.isin([11,12])) & (df["HourDK"].dt.hour.isin([17,18,19])) &
                   (df["HourDK"].dt.year==2019), "DSO_ImportTariff"] = 0.668
                    
            # 1/1/20 until 30/6/20
            df.loc[(df["HourDK"].dt.year==2020) & (df["HourDK"].dt.month<=6),
                   "DSO_ImportTariff"] = 0.2589
            df.loc[(df["HourDK"].dt.month.isin([1,2,3])) & (df["HourDK"].dt.hour.isin([17,18,19])) &
                   (df["HourDK"].dt.year==2020), "DSO_ImportTariff"] = 0.668
                    
            # 1/7/20 until 31/10/20
            df.loc[(df["HourDK"].dt.year==2020) & (df["HourDK"].dt.month.isin([7,8,9,10])),
                   "DSO_ImportTariff"] = 0.2431
            df.loc[(df["HourDK"].dt.month.isin([10])) & (df["HourDK"].dt.hour.isin([17,18,19])) &
                   (df["HourDK"].dt.year==2020), "DSO_ImportTariff"] = 0.6374
                    
            # 1/11/20 until 31/12/20
            df.loc[(df["HourDK"].dt.year==2020) & (df["HourDK"].dt.month.isin([11,12])),
                   "DSO_ImportTariff"] = 0.2024
            df.loc[(df["HourDK"].dt.month.isin([11,12])) & (df["HourDK"].dt.hour.isin([17,18,19])) &
                   (df["HourDK"].dt.year==2020), "DSO_ImportTariff"] = 0.5593
                    
            # 1/1/21 until 31/12/21
            df.loc[(df["HourDK"].dt.year==2021), "DSO_ImportTariff"] = 0.2363
            df.loc[(df["HourDK"].dt.month.isin([1,2,3,10,11,12])) & (df["HourDK"].dt.hour.isin([17,18,19])) &
                   (df["HourDK"].dt.year==2021), "DSO_ImportTariff"] = 0.6307
                                   
            # 1/1/22 until 31/3/22
            df.loc[(df["HourDK"].dt.year==2022) & (df["HourDK"].dt.month.isin([1,2,3])), 
                   "DSO_ImportTariff"] = 0.2363
            df.loc[(df["HourDK"].dt.month.isin([1,2,3])) & (df["HourDK"].dt.hour.isin([17,18,19])) &
                   (df["HourDK"].dt.year==2022), "DSO_ImportTariff"] = 0.6307
                    
            # 1/4/22 until 31/12/22
            df.loc[(df["HourDK"].dt.year==2022) & (df["HourDK"].dt.month>=4), 
                   "DSO_ImportTariff"] = 0.3003
            df.loc[(df["HourDK"].dt.month.isin([10,11,12])) & (df["HourDK"].dt.hour.isin([17,18,19])) &
                   (df["HourDK"].dt.year==2022), "DSO_ImportTariff"] = 0.7651
                    
            # Højlast for Jan 2023
            df.loc[(df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month==1),
                   "DSO_ImportTariff"] = 0.5103 
            # Lavlast for Jan 2023
            df.loc[(df["HourDK"].dt.hour.isin([0,1,2,3,4,5])) &
                   (df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month==1),
                   "DSO_ImportTariff"] = 0.1701
            # Spidslast for Jan 2023
            df.loc[(df["HourDK"].dt.hour.isin([17,18,19,20])) &
                   (df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month==1),
                   "DSO_ImportTariff"] = 1.5308
                
            # Højlast for Feb 2023
            df.loc[(df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month==2),
                   "DSO_ImportTariff"] = 0.5511 
            # Lavlast for Feb 2023
            df.loc[(df["HourDK"].dt.hour.isin([0,1,2,3,4,5])) &
                   (df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month==2),
                   "DSO_ImportTariff"] = 0.1837
            # Spidslast for Feb 2023
            df.loc[(df["HourDK"].dt.hour.isin([17,18,19,20])) &
                   (df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month==2),
                   "DSO_ImportTariff"] = 1.6533
            
            # Højlast for Mar 2023
            df.loc[(df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month==3),
                   "DSO_ImportTariff"] = 0.4528 
            # Lavlast for Mar 2023
            df.loc[(df["HourDK"].dt.hour.isin([0,1,2,3,4,5])) &
                   (df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month==3),
                   "DSO_ImportTariff"] = 0.1509
            # Spidslast for Mar 2023
            df.loc[(df["HourDK"].dt.hour.isin([17,18,19,20])) &
                   (df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month==3),
                   "DSO_ImportTariff"] = 1.3584
            
            # Højlast for Apr-May 2023
            df.loc[(df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month.isin([4,5])),
                   "DSO_ImportTariff"] = 0.2264
            # Lavlast for Apr-May 2023
            df.loc[(df["HourDK"].dt.hour.isin([0,1,2,3,4,5])) &
                   (df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month.isin([4,5])),
                   "DSO_ImportTariff"] = 0.1509
            # Spidslast for Apr-May 2023
            df.loc[(df["HourDK"].dt.hour.isin([17,18,19,20])) &
                   (df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month.isin([4,5])),
                   "DSO_ImportTariff"] = 0.5887      
     
            # Højlast for Jun-Jul-Aug-Sep 2023
            df.loc[(df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month.isin([6,7,8,9])),
                   "DSO_ImportTariff"] = 0.2264
            # Lavlast for Jun-Jul-Aug-Sep 2023
            df.loc[(df["HourDK"].dt.hour.isin([0,1,2,3,4,5])) &
                   (df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month.isin([6,7,8,9])),
                   "DSO_ImportTariff"] = 0.1509
            # Spidslast for Jun-Jul-Aug-Sep 2023
            df.loc[(df["HourDK"].dt.hour.isin([17,18,19,20])) &
                   (df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month.isin([6,7,8,9])),
                   "DSO_ImportTariff"] = 0.5887  
     
            # Højlast for Oct-Nov-Dec 2023
            df.loc[(df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month.isin([10,11,12])),
                   "DSO_ImportTariff"] = 0.4556
            # Lavlast for Oct-Nov-Dec 2023
            df.loc[(df["HourDK"].dt.hour.isin([0,1,2,3,4,5])) &
                   (df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month.isin([10,11,12])),
                   "DSO_ImportTariff"] = 0.1519
            # Spidslast for Oct-Nov-Dec 2023
            df.loc[(df["HourDK"].dt.hour.isin([17,18,19,20])) &
                   (df["HourDK"].dt.year==2023) & (df["HourDK"].dt.month.isin([10,11,12])),
                   "DSO_ImportTariff"] = 1.3668
            
            # Højlast for Jan-Feb-Mar 2024
            df.loc[(df["HourDK"].dt.year==2024) & (df["HourDK"].dt.month.isin([1,2,3])),
                   "DSO_ImportTariff"] = 0.4556
            # Lavlast for Oct-Nov-Dec 2023
            df.loc[(df["HourDK"].dt.hour.isin([0,1,2,3,4,5])) &
                   (df["HourDK"].dt.year==2024) & (df["HourDK"].dt.month.isin([1,2,3])),
                   "DSO_ImportTariff"] = 0.1519
            # Spidslast for Oct-Nov-Dec 2023
            df.loc[(df["HourDK"].dt.hour.isin([17,18,19,20])) &
                   (df["HourDK"].dt.year==2024) & (df["HourDK"].dt.month.isin([1,2,3])),
                   "DSO_ImportTariff"] = 1.3668  
            
        elif params["Area"] == "Bornholm":
            
            if params["Type"] == "B_høj":
                    
                df["Self_Con"] = 7.64/100
                    
                #pdb.set_trace()
                
                # Apply the same tariffs for all years now, fix later
                    
                # Start with setting all in Højlast
                df["DSO_ImportTariff"] = 10.55/100
                    
                # On weeekends and holidays in Summer, apply Lavlast
                df.loc[(df["season"] == "Summer") & ((df["is_weekend"] == True) | (df["is_holiday"] == True)),  
                       "DSO_ImportTariff"] = 3.52/100
                # Also apply Lavlast if hour is until 6
                df.loc[(df["HourDK"].dt.hour.isin([0,1,2,3,4,5])), "DSO_ImportTariff"] = 3.52/100
                    
                # During winter apply Spidslast from 6-21
                df.loc[(df["season"] == "Winter") & (df["HourDK"].dt.hour>=6) & (df["HourDK"].dt.hour<21), "DSO_ImportTariff"] = 21.09/100
            
        if params["taxes"] == False:
            df["Tax"] = 0
            
        if params["VAT"] == True:
            v = 1.25
        else:
            v = 1
            
        df['l_b'] = v*(df['SpotPriceDKK']/1000 + df['Tax'] + df['DSO_ImportTariff'] + df['TSO_ImportTariff'])
        df['l_s'] = df['SpotPriceDKK']/1000 - 1.25*df['TSO_ExportTariff']
        
    return df