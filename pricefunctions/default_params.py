def default_params():

    params = {}
    params["Area"] = "Radius"
    params["Type"] = "C"
    params["taxes"] = True
    params["VAT"] = True
    params["Self_Con"] = False
    params["res"] = 60          # time resolution in minutes
    params["DK_zone"] = "DK2"
    
    return params