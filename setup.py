# -*- coding: utf-8 -*-
"""
Created on Wed Nov  8 15:55:05 2023

@author: chazi
"""

import setuptools

setuptools.setup(
    name='pricefunctions',
    version='1',
    author='Haris Ziras',
    author_email='chazi@dtu.dk',
    description='A set of functions related to Danish electricity prices',
    url='https://gitlab.gbar.dtu.dk/chazi/pricefunctions',
    license='MIT',
    packages=['pricefunctions'],
    install_requires=['datetime', 'pytz', 'holidays', 'pandas', 'ipython'],
)